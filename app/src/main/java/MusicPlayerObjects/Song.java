package MusicPlayerObjects;

/**
 * Created by shannonlui on 2016-05-14.
 */
public class Song {
    private long id;
    private String title;
    private String artist;
    private String album;

    public Song(long songID, String songTitle, String songArtist, String songAlbum) {
        id=songID;
        title=songTitle;
        artist=songArtist;
        album=songAlbum;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public long getID() {
        return id;
    }

}
