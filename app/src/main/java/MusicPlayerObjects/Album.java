package MusicPlayerObjects;

import java.util.ArrayList;

/**
 * Created by shannonlui on 2016-05-16.
 */
public class Album {
    private String title;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String title, String artist) {
        this.artist = artist;
        this.title = title;
        songs = new ArrayList<Song>();
    }

    public String getArtist() {
        return artist;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public String getTitle() {
        return title;
    }

    public void addSong(Song song) {
        songs.add(song);
    }
}
